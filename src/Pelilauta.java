import java.util.*;

public class Pelilauta extends Numeropeli{

    private int[][] pelilauta;
    private ArrayList<Integer> lautaListana;

    public Pelilauta() {

        this.pelilauta = this.uusiLauta();

    }

    @Override
    public String toString() {

        String pelilautamerkkijonona = "";
        int pelilaattarivejä = 0;
        int lautaListaLaskuri = 0;
        for (int i = 1; i <= 4; i++) {
            pelilautamerkkijonona += "---------\n";
            if(pelilaattarivejä < 3) {
                for(int numeroita = 0; numeroita < 3; numeroita ++) {
                    pelilautamerkkijonona += "|" + this.lautaListana.get(lautaListaLaskuri) + "|";
                    lautaListaLaskuri ++;
                }
                pelilaattarivejä ++;
                pelilautamerkkijonona += "\n";
            }

        }

        int nollanIndeksi = pelilautamerkkijonona.indexOf("0");
        String pelilautaIlmanNollaa = pelilautamerkkijonona.substring(0, nollanIndeksi - 1) + "   "
                + pelilautamerkkijonona.substring(nollanIndeksi + 2);
        return pelilautaIlmanNollaa;
    }

    public int[][] uusiLauta() {

        int pelilauta[][] = new int[3][3];

        ArrayList<Integer> lautaListana = this.UusiLautaListana();

        int listanIndeksi = 0;
        System.out.println(lautaListana.toString());
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {

                pelilauta[i][j] = lautaListana.get(listanIndeksi);
                listanIndeksi ++;

            }
        }

        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {

                System.out.print(pelilauta[i][j]);

            }
            System.out.println();
        }

        this.lautaListana = lautaListana;
        return pelilauta;
    }

    public ArrayList<Integer> UusiLautaListana() {

        ArrayList<Integer> lautalista = new ArrayList();
        for (int i = 0; i < 9; i++) {
            lautalista.add(i);
        }

        System.out.println(lautalista);
        for (int i = 0; i < 20; i++) {

            int indeksi1 = (int) (Math.random() * 9);
            int indeksi2 = (int) (Math.random() * 9);

            int valiaikainen = lautalista.get(indeksi1);
            lautalista.set(indeksi1, lautalista.get(indeksi2));
            lautalista.set(indeksi2, valiaikainen);

        }

        return lautalista;

    }

    public void LaatanLiikutus(Scanner lukija) {

        int nollanRiviIndeksi = 0;
        int nollanSarakeIndeksi = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (pelilauta[i][j] == 0) {
                    nollanRiviIndeksi = i;
                    nollanSarakeIndeksi = j;
                    break;
                }
            }
        }

        boolean ylhaaltaSaaLiikuttaa = false;
        boolean alhaaltaSaaLiikuttaa = false;
        boolean vasemmaltaSaaLiikuttaa = false;
        boolean oikealtaSaaLiikuttaa = false;

        if(nollanRiviIndeksi != 0) {
            ylhaaltaSaaLiikuttaa = true;
        }
        if(nollanRiviIndeksi != 2) {
            alhaaltaSaaLiikuttaa = true;
        }
        if(nollanSarakeIndeksi != 0) {
            vasemmaltaSaaLiikuttaa = true;
        }
        if (nollanSarakeIndeksi != 2) {
            oikealtaSaaLiikuttaa = true;
        }

        int numeroYlhaalla = 0;
        if(ylhaaltaSaaLiikuttaa) {
            numeroYlhaalla = pelilauta[nollanRiviIndeksi - 1][nollanSarakeIndeksi];
        }
        int numeroAlhaalla = 0;
        if(alhaaltaSaaLiikuttaa) {
            numeroAlhaalla = pelilauta[nollanRiviIndeksi + 1][nollanSarakeIndeksi];
        }
        int numeroVasemmalla = 0;
        if(vasemmaltaSaaLiikuttaa) {
            numeroVasemmalla = pelilauta[nollanRiviIndeksi][nollanSarakeIndeksi - 1];
        }
        int numeroOikealla = 0;
        if(oikealtaSaaLiikuttaa) {
            numeroOikealla = pelilauta[nollanRiviIndeksi][nollanSarakeIndeksi + 1];
        }

        String liikkeetMerkkijonona = String.valueOf(numeroYlhaalla) + String.valueOf(numeroAlhaalla) + String.valueOf(numeroVasemmalla) + String.valueOf(numeroOikealla);

        System.out.print("Mahdolliset siirrot: ");
        for(int i = 0; i < 4; i++) {
            int mahdollinenLiike = Character.getNumericValue(liikkeetMerkkijonona.charAt(i));
            if (mahdollinenLiike > 0) {
                System.out.print(mahdollinenLiike + " ");

            }
        }
        System.out.println();
        System.out.print("Siirtosi: ");

        int pelaajanSiirto = 0;
        while(true) {

            pelaajanSiirto = Integer.valueOf(lukija.nextLine());

            if (pelaajanSiirto > 0 && (pelaajanSiirto == numeroYlhaalla || pelaajanSiirto == numeroAlhaalla
                    || pelaajanSiirto == numeroVasemmalla || pelaajanSiirto == numeroOikealla)) {
                break;
            }

            System.out.println("Laiton siirto! Kokeile uudestaan.");
        }

        int[][] uusiPelilauta = this.pelilauta;
        if(pelaajanSiirto == numeroYlhaalla) {
            uusiPelilauta[nollanRiviIndeksi][nollanSarakeIndeksi] = pelaajanSiirto;
            uusiPelilauta[nollanRiviIndeksi - 1][nollanSarakeIndeksi] = 0;
        } else if(pelaajanSiirto == numeroAlhaalla) {
            uusiPelilauta[nollanRiviIndeksi][nollanSarakeIndeksi] = pelaajanSiirto;
            uusiPelilauta[nollanRiviIndeksi + 1][nollanSarakeIndeksi] = 0;
        } else if(pelaajanSiirto == numeroVasemmalla) {
            uusiPelilauta[nollanRiviIndeksi][nollanSarakeIndeksi] = pelaajanSiirto;
            uusiPelilauta[nollanRiviIndeksi][nollanSarakeIndeksi - 1] = 0;
        } else if(pelaajanSiirto == numeroOikealla) {
            uusiPelilauta[nollanRiviIndeksi][nollanSarakeIndeksi] = pelaajanSiirto;
            uusiPelilauta[nollanRiviIndeksi][nollanSarakeIndeksi + 1] = 0;
        }

        setPelilauta(uusiPelilauta);

    }

    public void setPelilauta(int[][] pelilauta) {
        this.pelilauta = pelilauta;
        ArrayList<Integer> uusiLautaLista = new ArrayList();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                uusiLautaLista.add(pelilauta[i][j]);
            }
        }

        this.lautaListana = uusiLautaLista;
    }

    public boolean Voititko() {

        boolean voitto = false;
        String lautaMerkkijonona = "";

        for(int i = 0; i < lautaListana.size(); i++) {
            lautaMerkkijonona += lautaListana.get(i);
        }

        if(lautaMerkkijonona.equals("123456780")) {
            System.out.println("'``´´´¨¨''´´**´´``");
            System.out.println("| Voitit  pelin! |");
            System.out.println("´´´**'¨¨¨´*´´¨¨*''");
            voitto = true;
        }

        return voitto;
    }


}
